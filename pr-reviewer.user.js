// ==UserScript==
// @name         Console.log hunter
// @namespace    http://bitbucket.org/
// @version      0.2
// @description  Warn in console.log
// @author       JPFS
// @match        https://bitbucket.org/ingeniumpropietarios/*/pull-requests/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const TO_SEARCH = ['console.log'];
    //const TO_SEARCH = ['console.log', 'new IngeniumGridColumnComponent'];
    let findInterval = 0;

    const searchConsole = () => {
        console.log('start SEARCH!');
        $('.udiff-line').toArray().forEach((line) => {
            if ($(line).hasClass('addition') || $(line).hasClass('common')) {
                const content = $(line).find('pre.source');
                const finded = TO_SEARCH.reduce((x, a) => x || content.text().indexOf(a) >= 0, false);
                if (finded) {
                    content.attr('style', 'font-weight: bold; font-size: 1.1em; color: red;');
                }
            }
        });
    };

    function init() {
        findInterval = setInterval(() => {
            if ($('h1.filename').length) {
                clearInterval(findInterval);
                searchConsole();
            }
        }, 500);
    }

    init();
})();